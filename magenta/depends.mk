
define MODULE_DEPENDS.common
	(contrib-minIni,$(VROOT)/classico-magenta/contrib/minIni)
#	(nbu.contrib-openssl,$(VROOT)/nbu.contrib/open-ssl)
	(contrib-docopt,$(VROOT)/classico-magenta/contrib/docopt)
	(contrib-nlohmann-json,$(VROOT)/classico-magenta/contrib/nlohmann-json)
	(contrib-yaml-cpp,$(VROOT)/classico-magenta/contrib/yaml-cpp)
	(contrib-boost,$(VROOT)/classico-magenta/contrib/boost)
endef
