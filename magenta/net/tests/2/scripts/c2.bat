@echo off
: secure, without cert (will fail when client auth is required)

on break quit
setlocal
if "%certdir%"=="" set certdir=../cert/3
set ca=%certdir%/ca.pem

..\client\bin\rv-win32-debug\client2 -b -C %ca% %*
