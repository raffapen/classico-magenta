on break quit
setlocal
if "%certdir%"=="" set certdir=../cert/3
set cert=%certdir%/srv-cert.pem
set key=%certdir%/srv-prkey.pem
set ca=%certdir%/ca.pem
openssl s_server -key %key% -cert %cert% -CAfile %ca% -accept 8888 -verify_return_error -Verify 10
