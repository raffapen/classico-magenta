@echo off
: secure, without cert (will fail when client auth is required by server)

on break quit
setlocal
if "%certdir%"=="" set certdir=../cert/3
set cert=%certdir%/cli-cert.pem
set key=%certdir%/cli-prkey.pem
set ca=%certdir%/ca.pem

..\client\bin\rv-win32-debug\client2 -k %key -C %ca% %*
