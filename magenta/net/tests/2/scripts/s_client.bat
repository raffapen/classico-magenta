on break quit
setlocal
if "%certdir%"=="" set certdir=../cert/3
set cert=%certdir%/cli-cert.pem
set key=%certdir%/cli-prkey.pem
set ca=%certdir%/ca.pem
set host=127.0.0.1
set port=8888

openssl s_client -connect %host%:%port% -cert %cert% -key %key% %*
: -showcerts -msg
