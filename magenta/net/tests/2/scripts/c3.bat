@echo off
: secure (best effort: secure if other side is secure)

on break quit
setlocal
if "%certdir%"=="" set certdir=../cert/3
set cert=%certdir%/cli-cert.pem
set key=%certdir%/cli-prkey.pem
set ca=%certdir%/ca.pem

..\client\bin\rv-win32-debug\client2 -b -c %cert% -C %ca% -k %key% %*
