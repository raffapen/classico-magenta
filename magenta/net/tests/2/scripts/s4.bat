@echo off
: secure

on break quit
setlocal
if "%certdir%"=="" set certdir=../cert/3
set cert=%certdir%/srv-cert.pem
set key=%certdir%/srv-prkey.pem
set ca=%certdir%/ca.pem

..\server\bin\rv-win32-debug\server2 -c %cert% -C %ca% -k %key% %*
