on break quit
setlocal
if "%certdir%"=="" set certdir=../cert/3
set cert=%certdir%/cli-cert.pem
set key=%certdir%/cli-prkey.pem
set ca=%certdir%/ca.pem
set port=443
set host=127.0.0.1

curl -3 -v4ki --cert %cert% --cert-type PEM --key %key% --cacert %ca% https://%host%:%port%
