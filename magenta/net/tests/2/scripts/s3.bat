@echo off
: best effort (secure if other side is secure)

on break quit
setlocal
if "%certdir%"=="" set certdir=../cert/3
set cert=%certdir%/srv-cert.pem
set key=%certdir%/srv-prkey.pem
set ca=%certdir%/ca.pem

..\server\bin\rv-win32-debug\server2 -b -c %cert% -C %ca% -k %key% %*
